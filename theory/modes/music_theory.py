import os
from copy import deepcopy
from string import Template
import numpy as np


def to_roman(s, x):
    roman_numerals = ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII']
    roman_signature = []
    for idx, (si, xi) in enumerate(zip(s, x)):
        if si.isupper() or si.isdigit():
            next_element = roman_numerals[idx] + (si[:-1] if si.endswith('M') else si)
        else:
            next_element = roman_numerals[idx].lower() + si.replace('m', '')
        next_element = (xi[0] if xi.startswith('b') or xi.startswith('#') else '') + next_element
        roman_signature.append(next_element)
    return roman_signature


def to_latex(s):
    q = deepcopy(s)
    for i in range(len(q)):
        if q[i].endswith('o'):
            q[i] = '\\dimi{' + q[i][:-1] + '}'
        elif q[i].endswith('ø'):
            q[i] = '\\hdim{' + q[i][:-1] + '}'
        elif q[i].endswith('o7'):
            q[i] = '\\dims{' + q[i][:-2] + '}'
        elif q[i].endswith('M7'):
            q[i] = '\\MS{' + q[i][:-2] + '}'
        elif q[i].endswith('7'):
            q[i] = '\\DS{' + q[i][:-1] + '}'
        q[i] = q[i].replace('#', '$\\sharp$').replace('b', '$\\flat$')
    return q


def to_latex_sparse(s):
    q = deepcopy(s)
    for i in range(len(q)):
        if q[i].endswith('o'):
            q[i] = ''
        elif q[i].endswith('ø'):
            q[i] = ''
        elif q[i].endswith('M7'):
            q[i] = '\\MS{' + q[i][:-2] + '}'
        elif q[i].endswith('7'):
            q[i] = '\\DS{' + q[i][:-1] + '}'
        elif q[i].endswith('aug'):
            q[i] = ''
        q[i] = q[i].replace('#', '$\\sharp$').replace('b', '$\\flat$')
    return q


white_notes = ['C', 'D', 'E', 'F', 'G', 'A', 'B']
chromatic_scale = np.array(['C', 'Db', 'D', 'Eb', 'E', 'F', 'Gb', 'G', 'Ab', 'A', 'Bb', 'B'])
notes_index = ['C', 'C#', 'Db', 'D', 'Ebb', 'D#', 'Eb', 'E', 'Fb', 'E#', 'F', 'F#',
               'Gb', 'F##', 'G', 'G#', 'Ab', 'A', 'Bbb', 'A#', 'Bb', 'B', 'Cb', 'B#']
enharmonic_equivalents = {
    'C': 'B#', 'B#': 'C',
    'B': 'Cb', 'Cb': 'B',
    'Db': 'C#', 'C#': 'Db',
    'Eb': 'D#', 'D#': 'Eb',
    'F': 'E#', 'E#': 'F',
    'E': 'Fb', 'Fb': 'E',
    'Gb': 'F#', 'F#': 'Gb',
    'Ab': 'G#', 'G#': 'Ab',
    'Bb': 'A#', 'A#': 'Bb'}

extended_equivalents = {'A': 'Bbb', 'G': 'F##', 'D': 'Ebb'}
extended_equivalents.update(enharmonic_equivalents)

# TODO
# enharmonic_equivalents.update({
    # 'A': 'Bbb', 'A': 'G##',
    # 'B': 'A##', 'C': 'Dbb',
    # 'D': 'C##', 'D': 'Ebb',
    # 'E': 'D##', 'F': 'Gbb',
    # 'G': 'F##', 'G': 'Abb'})

chord_types = {'M': np.array([0, 4, 7]), 'm': np.array([0, 3, 7]),
               'o': np.array([0, 3, 6]), '+': np.array([0, 4, 8]),
               'sus2o': np.array([0, 2, 6]), 'sus4o': np.array([0, 4, 6]),
               'sus2ø': np.array([0, 2, 6, 10]), 'sus4ø': np.array([0, 4, 6, 10]),
               'm6': np.array([0, 3, 7, 9]), 'sus2o7': np.array([0, 2, 6, 9]),
               'M7': np.array([0, 4, 7, 11]), 'm7': np.array([0, 3, 7, 10]),
               '7': np.array([0, 4, 7, 10]), 'mM7': np.array([0, 3, 7, 11]),
               'o7': np.array([0, 3, 6, 9]), 'ø': np.array([0, 3, 6, 10]),
               '+M7': np.array([0, 4, 8, 11]), '+7': np.array([0, 4, 8, 10])}
inv_chord_types = {tuple(v): k for k, v in chord_types.items()}

chords_db = {(root_note + chord_type): chromatic_scale[(chord_spacing+idx) % 12]
             for idx, root_note in enumerate(chromatic_scale)
             for chord_type, chord_spacing in chord_types.items()}
inv_chords_db = {tuple(v): k for k, v in chords_db.items()}

major_modes = ['Ionian', 'Dorian', 'Phrygian', 'Lydian', 'Mixolydian',
               'Aeolian', 'Locrian']
melodic_minor_modes = [
    'Melodic Minor', 'Dorian b2', 'Lydian Augmented', 'Lydian Dominant',
    'Mixolydian b6', 'Aeolian b5', 'Superlocrian']
harmonic_minor_modes = [
    'Harmonic Minor', 'Locrian ♮6', 'Ionian #5', 'Dorian #4',
    'Phrygian Dominant', 'Lydian #2', 'Superlocrian bb7']
harmonic_major_modes = [
    'Harmonic Major', 'Dorian b5', 'Phrygian b4', 'Lydian b3', 'Mixolydian b2',
    'Lydian Augmented #2', 'Locrian bb7']
double_harmonic_major_modes = [
    'Double Harmonic Major Scale', 'Lydian #2 #6', 'Ultraphrygian',
    'Hungarian Minor', 'Oriental', 'Ionian #2 #5', 'Locrian bb3 bb7']
neapolitan_minor_modes = [
    'Neapolitan Minor', 'Lydian #6', 'Mixolydian Augmented', 'Romani Minor',
    'Locrian Dominant', 'Ionian #2', 'Ultralocrian bb3']
neapolitan_major_modes = [
    'Neapolitan Major', 'Leading Whole Tone', 'Lydian Augmented Dominant',
    'Lydian Dominant b6', 'Major Locrian', 'Half-Diminished b4',
    'Superlocrian bb3']


modes = []
modes += major_modes
modes += melodic_minor_modes
modes += harmonic_minor_modes
modes += harmonic_major_modes
modes += double_harmonic_major_modes
modes += neapolitan_minor_modes
modes += neapolitan_major_modes

alternative_names = {
    'Dorian': ('Λαϊκός Δρόμος Κιουρντί', 'Makam Ussak'),
    'Phrygian': ('Λαϊκός Δρόμος Ουσάκ'),
    'Phrygian Dominant': ('Λαϊκός Δρόμος Χιτζάζ', 'Makam Hicaz')
}

major_mode_intervals = [2, 2, 1, 2, 2, 2, 1]
mode_intervals = [major_mode_intervals[i:] + major_mode_intervals[:i]
                 for i in range(7)]
melodic_minor_intervals = [2, 1, 2, 2, 2, 2, 1]
mode_intervals += [melodic_minor_intervals[i:] + melodic_minor_intervals[:i]
                  for i in range(7)]
harmonic_minor_intervals = [2, 1, 2, 2, 1, 3, 1]
mode_intervals += [harmonic_minor_intervals[i:] + harmonic_minor_intervals[:i]
                   for i in range(7)]
harmonic_major_intervals = [2, 2, 1, 2, 1, 3, 1]
mode_intervals += [harmonic_major_intervals[i:] + harmonic_major_intervals[:i]
                   for i in range(7)]
double_harmonic_major_intervals = [1, 3, 1, 2, 1, 3, 1]
mode_intervals += [double_harmonic_major_intervals[i:] + double_harmonic_major_intervals[:i]
                   for i in range(7)]

neapolitan_minor_intervals = [1, 2, 2, 2, 1, 3, 1]
mode_intervals += [neapolitan_minor_intervals[i:] + neapolitan_minor_intervals[:i]
                   for i in range(7)]
neapolitan_major_intervals = [1, 2, 2, 2, 2, 2, 1]
mode_intervals += [neapolitan_major_intervals[i:] + neapolitan_major_intervals[:i]
                   for i in range(7)]

alterations = ['', '#', '##', 'bb', 'b']
mode_signatures = []
for i in range(len(mode_intervals)):
    diff = np.cumsum([mode_intervals[i][j] - major_mode_intervals[j] for j in range(6)])
    if any(di > 2 or di < -2 for di in diff):
        raise ValueError(f'Alterations should be in range [-2, 2], not {diff}')
    signature = ['1'] + [alterations[diff[j-2]] + str(j)  for j in range(2, 8)]
    mode_signatures.append(signature)

mode_chord_types = [[inv_chord_types[tuple(np.cumsum([0]+(interval*2)[m:])[:7][:5:2])] for m in range(7)] for interval in mode_intervals]
mode_seventh_chord_types = [[inv_chord_types[tuple(np.cumsum([0]+(interval*2)[m:])[:7][::2])] for m in range(7)] for interval in mode_intervals]

mode_chord_signatures = [to_roman(m, s) for m, s in zip(mode_chord_types, mode_signatures)]
mode_seventh_chord_signatures = [to_roman(m, s) for m, s in zip(mode_seventh_chord_types, mode_signatures)]

def build_scale(root_note, interval, idx):
    scale = [root_note]
    for offset in np.cumsum(interval)[:-1]:
        next_note = chromatic_scale[(idx+offset) % 12]
        if white_notes.index(next_note[0]) != (white_notes.index(scale[-1][0])+1)%7:
            next_note = enharmonic_equivalents[next_note]
        scale.append(next_note)
    return scale

def build_extended_scale(root_note, interval, idx):
    scale = [root_note]
    for offset in np.cumsum(interval)[:-1]:
        next_note = chromatic_scale[(idx+offset) % 12]
        if white_notes.index(next_note[0]) != (white_notes.index(scale[-1][0])+1)%7:
            next_note = extended_equivalents[next_note]
        scale.append(next_note)
    return scale


scales = {}
chords = {}
seventh_chords = {}
for mode, interval, ch_types, sch_types in zip(modes, mode_intervals, mode_chord_types, mode_seventh_chord_types):
    scales[mode] = {}
    chords[mode] = {}
    seventh_chords[mode] = {}
    for idx, root_note in enumerate(chromatic_scale):
        actual_root_note = root_note
        try:
            scales[mode][actual_root_note] = build_scale(actual_root_note, interval, idx)
        except KeyError:
            try:
                actual_root_note = enharmonic_equivalents[actual_root_note]
                scales[mode][actual_root_note] = build_scale(actual_root_note, interval, idx)
            except KeyError:
                scales[mode][actual_root_note] = build_extended_scale(actual_root_note, interval, idx)

        chords[mode][actual_root_note] = [n+(ct if ct != 'M' else '') for n, ct in zip(scales[mode][actual_root_note], ch_types)]
        seventh_chords[mode][actual_root_note] = [n+ct for n, ct in zip(scales[mode][actual_root_note], sch_types)]

        num_sharps_or_flats = len([1 for n in scales[mode][actual_root_note]
                                   if 'b' in n or '#' in n])
        if num_sharps_or_flats >= 5:
            try:
                new_root_note = enharmonic_equivalents[actual_root_note]
                scales[mode][new_root_note] = build_scale(new_root_note, interval, idx)
            except KeyError:
                print('No enharmonic equivalent for {0} except {1}'.format(actual_root_note, build_extended_scale(new_root_note, interval, idx)))
                continue

            chords[mode][new_root_note] = [n+(ct if ct != 'M' else '') for n, ct in zip(scales[mode][new_root_note], ch_types)]
            seventh_chords[mode][new_root_note] = [n+ct for n, ct in zip(scales[mode][new_root_note], sch_types)]

def just_and_join(s, width=20):
    return  ' & ' + ' & '.join([si.ljust(width) for si in s[:-1]]) + ' & ' + s[-1]


def generate_modes_charts():
    all_modes_charts = []

    if not os.path.exists('modes'):
        os.mkdir('modes')

    for idx, mode in enumerate(modes):
        latex_mode_signature = [msi.replace('#', '$\\sharp$').replace('b', '$\\flat$')
                                for msi in mode_signatures[idx]]
        latex_chord_signature = to_latex(mode_chord_signatures[idx])
        latex_seventh_signature = to_latex(mode_seventh_chord_signatures[idx])
        signature_substitution = (9 * ' '
                                + just_and_join(latex_mode_signature)
                                + '\n           '
                                + just_and_join(latex_chord_signature)
                                + '\n           '
                                + just_and_join(latex_seventh_signature)
                                + '\n  \\\\')
        mode_keys = []
        for root_note in sorted(scales[mode], key=lambda x: notes_index.index(x)):
            latex_scales = to_latex(scales[mode][root_note])
            latex_chords = to_latex(chords[mode][root_note])
            latex_seventh_chords = to_latex(seventh_chords[mode][root_note])
            mode_keys.append(root_note.replace('#', '$\\sharp$').replace('b', '$\\flat$').ljust(9)
                            + just_and_join(latex_scales)
                            + '\n           '
                            + just_and_join(latex_chords)
                            + '\n           '
                            + just_and_join(latex_seventh_chords)
                            + '\n  \\\\')
        latex_mode_name = mode.replace('#', '$\\sharp$').replace('♮', '$\\natural$')
        latex_mode_name = ' '.join([word.replace('b', '$\\flat$')
                                    if any(chi.isdigit() for chi in word)
                                    else word
                                    for word in latex_mode_name.split(' ')])
        substitution_dict = {'modename': mode.replace('#', '\#'),
                             'latexmodename': latex_mode_name,
                             'modesignature': signature_substitution,
                             'modekeys': '\n  '.join(mode_keys)}

        with open('mode_chart_template.txt', 'r') as f:
            src = Template(f.read())
            out = src.substitute(substitution_dict)

        with open(os.path.join('modes', f'{mode.lower().replace(" ", "_")}.tex'), 'w') as f:
            f.write(out)

        with open('single_mode_chart_template.txt', 'r') as f:
            src = Template(f.read())
            out = src.substitute(substitution_dict)
            all_modes_charts.append(out)

    with open('all_modes_chart_template.txt', 'r') as f:
        src = Template(f.read())
        final_out = src.substitute({'allmodes': '\n'.join(all_modes_charts)})

    with open(os.path.join('modes', 'all_modes.tex'), 'w') as f:
        f.write(final_out)


def generate_sparse_modes_charts():
    for idx, mode in enumerate(modes):
        latex_mode_signature = [msi.replace('#', '$\\sharp$').replace('b', '$\\flat$')
                                for msi in mode_signatures[idx]]
        latex_chord_signature = to_latex(mode_chord_signatures[idx])
        # latex_seventh_signature = to_latex(mode_seventh_chord_signatures[idx])
        signature_substitution = (9 * ' '
                                + just_and_join(latex_mode_signature)
                                + '\n           '
                                + just_and_join(latex_chord_signature)
                                # + '\n           '
                                # + just_and_join(latex_seventh_signature)
                                + '\n  \\\\')
        mode_keys = []
        for root_note in sorted(scales[mode], key=lambda x: notes_index.index(x)):
            latex_scales = to_latex_sparse(scales[mode][root_note])
            latex_chords = to_latex_sparse(chords[mode][root_note])
            # latex_seventh_chords = to_latex_sparse(seventh_chords[mode][root_note])
            mode_keys.append(root_note.replace('#', '$\\sharp$').replace('b', '$\\flat$').ljust(9)
                            + just_and_join(latex_scales)
                            + '\n           '
                            + just_and_join(latex_chords)
                            # + '\n           '
                            # + just_and_join(latex_seventh_chords)
                            + '\n  \\\\')
        substitution_dict = {'modename': mode,
                            'modesignature': signature_substitution,
                            'modekeys': '\n  '.join(mode_keys)}

        with open('sparse_mode_chart_template.txt', 'r') as f:
            src = Template(f.read())
            out = src.substitute(substitution_dict)

        if not os.path.exists('sparse_modes'):
            os.mkdir('sparse_modes')
        with open(os.path.join('sparse_modes', f'{mode.lower().replace(" ", "_")}.tex'), 'w') as f:
            f.write(out)


generate_modes_charts()
generate_sparse_modes_charts()
