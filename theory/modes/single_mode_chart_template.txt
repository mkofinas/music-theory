\section*{\centering ${latexmodename} Mode}

\begin{adjustbox}{max width=\textwidth}
  \begin{tabular}{l*{7}{A}*{7}{B}*{7}{C}}
    \toprule
    Root & \multicolumn{7}{c}{Notes} & \multicolumn{7}{c}{Triad Chords} & \multicolumn{7}{c}{7\textsuperscript{th} Chords}\\
    \cmidrule(lr){2-8}\cmidrule(lr){9-15}\cmidrule(lr){16-22}
    ${modesignature}
    \midrule
    ${modekeys}
    \bottomrule
  \end{tabular}
\end{adjustbox}
